.. _troubleshooting:

###############
Troubleshooting
###############

In this section, we give some troubleshooting hints. Continue by choosing the component.

.. toctree::
   :maxdepth: 2

   xuc/index
   config/index
   recording/index
   spagobi/index
   kibana/index
   nginx/index

