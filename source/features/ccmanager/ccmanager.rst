.. highlightlang:: rest

.. _ccmanager:

#########################
Contact center management
#########################

============
Introduction
============

.. figure:: ccmanager.png
    :scale: 50%

CCmanager is a web application to manage and supervise a contact center


* Display queues
* Display agents / agents status
* Move or add agents in queue / penalty
* Move of add group of agents in queue / penalty
* Action on agents
    * Login / Logout
    * Pause / Unpause
    * Listen [#f1]_


Start the application : http://<xuc:port>/ccmanager

Single Agent Edition
********************

.. figure:: agent-edit.png
    :scale: 50%

This interface allows a user to change queue assignement and the associated penalty. The queue table display the following columns

* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue
* "default": The default penalty for the corresponding queue

The queue/active penalty couples can be saved as default configuration by clicking the "Set default" button, then "Save".
The queue/default penalty couples can be saved as active configuration by clicking the "Set current" button, then "Save".

Notes
-----

* Emptying the penalty textbox and saving will remove the queue from the active configuration for the agent.
* Emptying the default textbox and saving will remove the queue from the default configuration for the agent.

Multiple Agent Selection
************************

From agent view you are able to add or remove more than one agent at the same time.

.. figure:: multipleagentselect.png
    :scale: 80%

Once the agent selection is done, click on the edit button to display the configuration window

.. figure:: agentsconfiguration.png
    :scale: 80%

Click on the plus button to add a queue for selection, click on the minus button to remove a queue to the selection.
Once queue to add or removed are choosen, click on save button to apply your configuration change.

Click on "Apply default configuration" to apply existing default configuration to all selected users and make it the active configuration. This action only affects users with an existing default configuration, agents whithout default configuration remain unchanged.

Create base configuration for a set of agents
*********************************************

From the agent view, after selecting one or more agents, you can create a base configuration by clicking on one of the menu item in the following drop down:

.. figure:: ccmanager-base-batch-dropdown.png
   :scale: 80%

* 'Create base configuration' will allow you to create a base configuration from scratch for all the selected agents.
* 'Create base configuration from active configuration' will allow you to create a base configuration using the selected agents active configuration. The queue membership and penalty populated will be built based on the merged membership of all the selected agents. In case of conflict, the lowest penalty will be used.
    
In both cases, you will be able to review your changes before applying them. The 'Create base configuration' popup is similar to the single agent edition popup:

.. figure:: ccmanager-base-batch-create.png
   :scale: 80%
		 
The queue table display the following columns:
	   
* "Number": The queue number
* "Name": The queue name
* "Penalty": The active penalty for the corresponding queue

Click on the plus button to add a queue for selection. Once your configuration is complete, click on save button to apply your configuration change.

Thresholds
**********

Color thresholds can be defines for the waitinig calls counter and the maximum waiting time counter

.. figure:: ccmanager_thresholds.png
    :scale: 80%

Applys to the queue view and the global view

.. _ccmanager_callbacks:

=========
Callbacks
=========

This view allows to manage callback request : importing a new list of callbacks, monitoring them and downloading the associated tickets.

.. figure:: ccmanager_callbacks.png
    :scale: 80%

Callbacks can be imported from a CSV file into a :ref:`callback list <callback_lists>`.
The file must look like the following:

::

    phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|dueDate|period
    0230210092|0689746321|John|Doe|MyCompany|Call back quickly||
    0587963214|0789654123|Alice|O'Neill|YourSociety||2016-08-01|Afternoon

The header line must contain the exact field named described below:
    
- phoneNumber: The number to call (at least either phoneNumber or mobilePhoneNumber is required)
- mobilePhoneNumber: Alternate number to call
- firstName: The contact first name (optional)
- lastName: The contact last name (optional)
- company: The contact company name (optional)
- description: A text that will appear on the agent :ref:`callback pane <agent_callbacks>`
- dueDate: The date when to callback, using ISO format: YYYY-MM-DD, ie. 2016-08-01 for August, 1st, 2016. If not present the next day will be used as dueDate (optional)
- period: The name of the period as defined in :ref:`callback list <callback_lists>`. If not present, the default period will be used (optional)
  

When an agent takes a callback, the column ``Taken by`` is updated with the number of the aget. The callback disappears when it is processed.
The tickets of the processed callbacks can be downloaded by clicking on the ``Download tickets`` button.

.. rubric:: Footnotes
.. [#f1] Only supervisors which have their own lines can listen to agents, no supported on mobile supervisors, a line has to be affected to supervisors in xivo
