########################################
Xuc Xivo Unified Communication Framework
########################################

Xuc is an application suite developed by Avencall_ Group, based on several free existing components
including XiVO_, and our own developments to provide communication services api and application to businesses.
Xuc is build on Play_ using intensively Akka_ and written in Scala_

.. _Avencall: http://www.avencall.com/
.. _XiVO: http://documentation.xivo.io/
.. _Play: http://www.playframework.com/
.. _Akka: http://akka.io/
.. _Scala: http://www.scala-lang.org/


XiVO is `free software`_. Most of its distinctive components, and Xuc as a whole, are distributed
under the *LGPLv3 license*.

.. _free software: http://www.gnu.org/philosophy/free-sw.html

Xuc is providing

* Javascript API
* Web services
* Sample application
* Simple agent application
* Simple unified communication application pratix
* Contact center supervision
* Contact center statistics

The proposed applications are available in English and French. The list of preferred langs sent by the browser is analyzed and the first known lang is used, so if the browser requests it, en and fr the page will be server in en. The fallback language is French. Contributions are welcome, start with opening an issue on gitlab project page.

Xuc is composed of 3 modules

* The server module
* The core module
* The statistic module.

.. toctree::
   :maxdepth: 2

   developers/developers
   api/javascriptapi
   api/rest
   stats/statistics
   technical/structure

