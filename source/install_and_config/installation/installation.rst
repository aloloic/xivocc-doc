.. _installation:

**************
CTI components
**************

In order for these components to be fully functional, some customizations need to be done on the XiVO:
they will all be covered in a first part.

Overview
========

The following components will be installed :

- XuC : outsourced CTI server providing telephony events, statistics and commands through a WebSocket
- XuC Management : supervision web pages based on the XuC
- Pack Reporting : statistic summaries stored in a PostgreSQL database
- Totem Support : near-real time statistics based on ElasticSearch_
- SpagoBI : BI suite with default statistic reports based on the Pack Reporting
- Recording Server : web server allowing to search recorded conversations
- Xuc Rights Management : permission provider used by XuC and Recording Server to manage the user rights

.. _ElasticSearch: https://www.elastic.co/

Install from repository
=======================

There is a package *xivocc-installer* available in the repository which will configure XiVO PBX and install XiVO CC.

.. warning:: The Xivo is reconfigured during the installation and must be restarted, you may accept the automatic
    restart during the installation or you need to restart it manually later before starting the docker containers.


Install process
---------------

The install process from the repository consists of four parts:

    * The first part is to manually add the Avencall repository. You may contact Avencall to get access.
    * The second part is to manually run the prerequisites script to install docker and docker compose.
    * The third part is the installation process itself.
    * The fourth part is to install the package for the recording.

The installation is automatic and you will be asked few questions during the process.

    * When asked to generate a pair of authentication keys, leave the password field empty.
    * Before copying the authentication keys, you will be prompted for the XiVO PBX root password.
    * XiVO PBX must restart, the question will prompt you to restart during the process or to restart later.

Install Docker and Docker Compose
---------------------------------

Download script which will install docker and docker compose.

.. code-block:: ini

    wget https://gitlab.com/xivoxc/packaging/raw/2016.02/install/install-docker.sh -O install-docker.sh
    chmod +x install-docker.sh
    ./install-docker.sh

Package Installation
--------------------

Install the *xivocc-installer* package via *apt*. It is required to restart XiVO PBX during or after the setup process.
The installer will ask whether you wish to restart XiVO PBX later.

.. code-block:: ini

    apt-get install xivocc-installer

Package for the recording
-------------------------

To use the recording feature, you must install on the XiVO PBX the debian package available in the repository.

.. code-block:: ini

    apt-get install xivo-recording

After-install steps
-------------------

After the successful installation, start docker containers by an alias which was added to ~/.bashrc

.. code-block:: ini

    source ~/.bashrc
    dcomp -d up

If you selected to restart XiVO PBX later, please do so when possible to apply the modifications made by the installer.
The XUC server will not be able to connect correctly to the database on XiVO PBX.

To restart XiVO services, on XiVO PBX server run

.. code-block:: ini

    xivo-service restart all


Known Issues
------------

To avoid problems when uninstalling, you should:
    * to uninstall, please use apt-get purge xivocc-installer
    * if the process is aborted, it will break the installation, please *apt-get purge* and *apt-get install* again



Prerequisites
=============

We will assume your outsourced server meets the following requirements:

- OS : Debian 8 (jessie), 64 bit
- Docker_ installed
- Docker-compose_ installed
- the XiVO PBX is reachable on the network
- the XiVO PBX is setup with users, queues and agents, you must be able to place and answer calls.

.. _Docker: https://www.docker.com
.. _Docker-compose: https://docs.docker.com/compose/install/

Note : Install only released version of docker and docker compose

We will make the following assumptions :

- the XiVO has the IP 192.168.0.1
- some data (incoming calls, internal calls etc.) might be available on XiVO (otherwise, you will not see `anything` in the check-list_ below).
- the server has the IP 192.168.0.2
- the latest version of Docker_ is installed
- the latest version of Docker-compose_ is installed
- the package xivo-recording is available on a custom Debian mirror. If this is not the case, you will need to skip the `apt-get install` commands and build the packages yourself.

Install ntp server
------------------
.. code-block:: ini

    apt-get install ntp

XUC the server and the XiVO server must be synchronized to the same source.

Enable Docker LogRotate
-----------------------

Docker container log output to /dev/stdout and /dev/stderr.
The Docker container log file is saved in /var/lib/docker/containers/[CONTAINER ID]/[CONTAINER_ID]-json.log.

Create a new Logrotate config file for your Docker containers in the Logrotate folder /etc/logrotate.d/docker-container.

.. code-block:: ini

        /var/lib/docker/containers/*/*.log {
          rotate 7
          daily
          compress
          missingok
          delaycompress
          copytruncate
        }

You can test it with logrotate -fv /etc/logrotate.d/docker-container.
You should get some output and a new log file with suffix [CONTAINER ID]-json.log.1 should be created.
This file is compressed in next rotation cycle.

XiVO configuration
==================

PostgreSQL configuration
------------------------

Firstly, allow access to PostgreSQL from the outside. Edit `/etc/postgresql/9.1/main/postgresql.conf`:

.. code-block:: ini

   listen_addresses = '*'

Add this line to `/etc/postgresql/9.1/main/pg_hba.conf`:

.. code-block:: ini

   host asterisk all 192.168.0.2/32 md5

Create a user `stats` with read permissions :

.. code-block:: bash

   sudo -u postgres psql asterisk << EOF
   CREATE USER stats WITH PASSWORD 'stats';
   GRANT SELECT ON ALL TABLES IN SCHEMA PUBLIC TO stats;
   EOF

And run `xivo-service restart all` to apply these modifications.

AMI configuration
-----------------

* Xivo < 15.18 Add a new user in `/etc/asterisk/manager.conf` with :
* Xivo >= 15.18 Add a a file xuc.conf in `/etc/asterisk/manager.d` directory with :

.. code-block:: ini

   [xuc]
   secret = xucpass
   deny=0.0.0.0/0.0.0.0
   permit=X.X.X.0/255.255.255.0
   read = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan
   write = system,call,log,verbose,command,agent,user,dtmf,originate,dialplan

Replace X.X.X.0 by your xivocc network

And reload the AMI :

.. code-block:: bash

   asterisk -rx "manager reload"
   asterisk -rx "manager show user xuc" and check your if previous configuration is displayed.

CEL Configuration
-----------------

Add some events in the CEL. Edit `/etc/asterisk/cel.conf`:

- For Asterisk 11:

.. code-block:: ini

   [general]
   enable=yes
   apps=dial,park,queue
   events=APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_START,BRIDGE_END,BRIDGE_UPDATE,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled=yes

- For Asterisk 13:

.. code-block:: ini

   [general]
   enable = yes
   apps = dial,park,queue
   events = APP_START,CHAN_START,CHAN_END,ANSWER,HANGUP,BRIDGE_ENTER,BRIDGE_EXIT,USER_DEFINED,LINKEDID_END,HOLD,UNHOLD,BLINDTRANSFER,ATTENDEDTRANSFER

   [manager]
   enabled = yes

and reload the cel module in Asterisk :

.. code-block:: bash

   asterisk -rx "module reload cel"

Customizations in the web interface
-----------------------------------

Create a user Xuc in *Services -> IPBX -> Users* with the following parameters:

- CTI login : xuc
- CTI password : 0000
- profil supervisor

Create a Web Services user in *Configuration -> Web Services Access* with the following parameters :

- Login : xivows
- Password : xivows
- Host : 192.168.0.2

Make sure **Multiqueues call stats sharing** is enabled in *Services -> Ipbx -> Advanced configuration* tab.

Phone integration
-----------------
Do not forget to follow configuration steps detailed in :ref:`Required configuration for phone integration
<phone_integration_installation>`.

Packages for the recording
--------------------------

Still on the xivo, install the package which will handle the recording :

.. code-block:: bash

   apt-get update
   apt-get install xivo-recording

During the installation, you will be asked for :

- the recording server IP (i.e. 192.168.0.2) 
- and the XiVO name (it **must** not contain any space or "-" character).

If you have several XiVO, you **must** give a different name to each of them.

This package has installed two dialplan sub-routines :

- xivo-incall-recording : used to record incoming calls
- xivo-outcall-recording : used to record outgoing calls

You have to manually place them where you want.

If you want to record on a gateway used with Xivo, you must not use the xivo-recording package but gateway-recording.

If you want to use call recording filtering, please install also:

.. code-block:: bash

    apt-get install call-recording-filtering
    
During the installation, you will be asked for :

- the recording server address with protocol and port (i.e. http://192.168.0.2:9400)     

XiVO CC
=======

Now we switch to the installation of the XiVO CC server.

Retrieve the configuration script and launch it:

.. code-block:: bash

   wget  https://gitlab.com/xivoxc/packaging/raw/master/install/install-docker-xivocc.sh
   bash install-docker-xivocc.sh

During the installation, you will be asked for :

- the XiVO IP address (e.g. 192.168.0.1)
- the number of weeks to keep for the statistics
- the number of weeks to keep for the recording files
- the external IP of the machine (i.e. the adress used afterwards for http URLs)

Create the following alias in your .bashrc file:

.. code-block:: bash

    vi ~/.bashrc
    alias dcomp='docker-compose -p xivocc -f /etc/docker/compose/docker-xivocc.yml'

Xivo release <= 15.12 (asterisk 11)
-----------------------------------

Edit the */etc/docker/compose/docker-xivocc.yml* and replace the image tag for **xuc** and **xivo_stats** with *latestast11*:

.. code-block:: yaml

    ...
    xivo_stats:
        image: xivoxc/xivo-full-stats:latestast11 <---------------- TO BE REPLACED ------------
    ...

    xuc:
        image: xivoxc/xuc:latestast11 <---------------- TO BE REPLACED ------------

    ...

Xivo release > 15.12 (asterisk 13)
----------------------------------

In */etc/docker/compose/docker-xivocc.yml*, check that the image tag for **xuc** and **xivo_stats** is *latestast13*:

.. code-block:: yaml

    ...
    xivo_stats:
        image: xivoxc/xivo-full-stats:latestast13 <---------------- TO BE CHECKED ------------
    ...

    xuc:
        image: xivoxc/xuc:latestast13 <---------------- TO BE CHECKED ------------

    ...

Xivo release = 16.03
----------------------------------

In */etc/docker/compose/docker-xivocc.yml*, check that the image tag for **xuc** is *latestxivo16*:

.. code-block:: yaml

    ...

    xuc:
        image: xivoxc/xuc:latestxivo16 <---------------- TO BE CHECKED ------------

    ...

Starting XivoCC
---------------

Then you can launch the XiVO CC with the following command :

.. code-block:: bash

   dcomp up -d


List XivoCC services :


.. code-block:: bash


    # dcomp ps
               Name                         Command               State                        Ports
    ---------------------------------------------------------------------------------------------------------------------
    xivocc_config_mgt_1         bin/config-mgt-docker            Up       0.0.0.0:9100->9000/tcp
    xivocc_elasticsearch_1      /docker-entrypoint.sh elas ...   Up       0.0.0.0:9200->9200/tcp, 0.0.0.0:9300->9300/tcp
    xivocc_fingerboard_1        /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_kibana_volumes_1     /bin/sh -c /usr/bin/tail - ...   Up
    xivocc_nginx_1              nginx -g daemon off;             Up       443/tcp, 0.0.0.0:80->80/tcp
    xivocc_pack_reporting_1     /bin/sh -c echo            ...   Up
    xivocc_pgxivocc_1           /docker-entrypoint.sh postgres   Up       0.0.0.0:5443->5432/tcp
    xivocc_postgresvols_1       /bin/bash                        Exit 0
    xivocc_recording_server_1   bin/recording-server-docker      Up       0.0.0.0:9400->9000/tcp
    xivocc_reporting_rsync_1    /usr/local/sbin/run-rsync.sh     Up       0.0.0.0:873->873/tcp
    xivocc_spagobi_1            /bin/sh -c /root/start.sh        Up       0.0.0.0:9500->8080/tcp
    xivocc_timezone_1           /bin/bash                        Exit 0
    xivocc_xivo_replic_1        /usr/local/bin/start.sh /o ...   Up
    xivocc_xivo_stats_1         /usr/local/bin/start.sh /o ...   Up
    xivocc_xivocclogs_1         /bin/bash                        Exit 0
    xivocc_xuc_1                bin/xuc_docker                   Up       0.0.0.0:8090->9000/tcp
    xivocc_xucmgt_1             bin/xucmgt_docker                Up       0.0.0.0:8070->9000/tcp

Checking Installed Version
--------------------------

Compoment version can be find in the log files, on the web pages for web components. You may also get the version from the docker container itself by typing :

.. code-block:: bash

    docker exec -ti xivocc_xucmgt_1 cat /opt/docker/conf/appli.version

Change xivocc_xucmgt_1 by the component version you want to check

Using XivoCC
------------

The various applications are available on the following addresses:

.. figure:: fingerboard.png
   :scale: 100%


- Xuc-related applications: http://192.168.0.2:8070/
- SpagoBI: http://192.168.0.2:9500/
- Config Management: http://192.168.0.2:9100/
- Recording server: http://192.168.0.2:9400/
- Kibana: http://192.168.0.2/

Post Installation
=================

User Configuration
------------------

* Using the configuration manager : http://192.168.0.2:9100/ (default user avencall/superpass)  add a user to be able to use the recording interface with proper rights.

Note: Xuc server default user is xuc, add xuc as administrator to be able to get call history in web assistant.


SpagoBi
-------

- Go to http://192.168.0.2:9500/SpagoBI (by default login: biadmin, password: biadmin)
- Update default language : go to "⚙ Resources" > "Configuration management" > in the "Select Category" field, chose "LANGUAGE_SUPPORTED" and change value of the label "SPAGOBI.LANGUAGE_SUPPORTED.LANGUAGE.default" in your language : fr,FR , en,US , ...
- Download the standard reports from https://gitlab.com/xivocc/sample_reports/raw/master/spagobi/standardreports.zip
- Import zip file in SpagoBI, all default options, with Jasper Report Engine as Engine associations.

XivoCC Default Report Sample
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: reportsample.png
    :scale: 50%


Use the database status report to check if replication and reporting generation is working :

.. figure:: datastatus.png
   :scale: 100%


ACD outgoing calls
------------------

XivoCC agent can make outgoing calls through an outgoing queue. This brings the statistics and supervision visualization for outgoing ACD calls. However, some special configuration steps are required:

- You need to create an outgoing queue with a name starting with 'out', e.g. outgoing_queue.
- This queue must be configured with preprocess subroutine xuc_outcall_acd, without On-Hold Music (tab General), Ringing Time must be 0 and Ring instead of On-Hold Music must be activated (both tab Application).  
- The subroutine must be deployed on the Xivo server (to /etc/asterisk/extension_extra.d/ or through the web interface), the file is available from https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/xuc_outcall_acd.conf, with owner asterisk:www-data and rights 660.
- You must also deploy the file https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/generate_outcall_skills.py to /usr/local/sbin/, with owner root:root and rights 755.
- Furthermore, you must replace the file /etc/asterisk/queueskills.conf by the following one https://gitlab.com/xivoxc/xucserver/raw/master/xivo/outbound/queueskills.conf (be sure to backup the original one), without changing the owner or rights
- And finally you need to add a new skill rule on the Xivo server: Services -> Call center -> Skill rules -> Add, with name 'select_agent' and rules '$agent > 0'.

Once done, calls requested by an agent through the Cti.js with more than 6 digits are routed via the outgoing queue. You can change the number of digits using the parameter xuc.outboundLength in the xuc's configuration.

Totem Panels
------------

Data replication can take some time if there are a lot of data in xivo cel and queue log tables. You may check xivo-db-replication log files (/var/log/xivocc/xivo-db-replication.log).

Preconfigured panels are available on  http://@IP/kibana/#/dashboard/file/queues.json et http://@IP/kibana/#/dashboard/file/agents.json to be able to save this panels in elasticsearch database you have
to sign on on request user admin/Kibana

.. figure:: totempanel.png
    :scale: 50%

.. _check-list:

Post Installation Check List
----------------------------

- All components are running : dcomp ps
- Xuc internal database is synchronized with xivo check status page with http://xivoccserver:8090/
- CCManager is running, log a user and check if you can see and manage queues : http://xivoccserver:8070/ccmanager
- Web agent is running, log an agent and check if you can change the status : http://xivoccserver:8070/agent
- Web assistant is running, and you get call history : http://xivoccserver:8070/
- Check database replication status using spagobi system report
- Check elasticsearch database status (totem panels) http://xivoccserver:9200/queuelogs/_status
- Check that you can listen to recordings http://xivoccserver:9400/
- Check totem panels http://192.168.85.102/kibana

###### reminder: Make sure to have few calls made in your XiVO, despite you will not see **anything** in totem or spagobi. 

Ldap Authentication
===================

Xuc
---

Configure LDAP authent for CCmanager, Web Assistant and Web Agent

You need to include in the compose.yml file a link to a specific configuration file by adding in xuc section a specific volume
and an environment variable to specify the alternate config file location

::

   xuc:

   ....

   environment:
   ....
   - CONFIG_FILE=/conf/xuc.conf

   volumes:
   - /etc/docker/xuc:/conf


Edit in /etc/docker/xuc/ a configuration file named xuc.conf to add ldap configuration (empty by default)


::

   include "application.conf"

   authentication {
     ldap {
       managerDN = "uid=company,ou=people,dc=company,dc=com"      # user with read rights on the whole LDAP
       managerPassword = "xxxxxxxxxx"                             # password for this user
       url = "ldap://ldap.company.com:389"                        # ldap URI
       searchBase = "ou=people,dc=company,dc=com"                 # ldap entry to use as search base
       userSearchFilter = "uid=%s"                                # filter to use to search users by login, using a string pattern
     }
   }

Recreate the container : `dcomp up -d xuc`

webRTC
======

Xuc
---

For the moment available only on the sample page, pre-configured to be used on LAN without ICE for NAT traversal.
Once logged on the sample page, you can init the webRTC through the init button, follow events shown in the webRTC section
and send and receive calls. You can terminate a call by the terminate button in the phone section. Direct and attended transfer
can be performed using phone section methods. Hold and DTMF features are available via the webRTC API. Current implementation
support just one simultaneous call.

Current browsers doesn't allow media sharing without secure connections - https and wss. The xivoxc_nginx docker image
contains the configuration required for loading the sample page over a secure connection using an auto-signed certificate.
This certificate is automatically generated by the installation script. It is meant to be used only for test purposes,
you should replace it by a signed certificate before switching to production. The sample page is available on the
following address: https://MACHINE_IP:8443/sample

Xivo
----

Awaiting integration of the additional SIP options to the Xivo's web interface you need to add manually webRTC peers to the
sip.conf in the `/etc/asterisk/sip.conf` file, after the current content. You must also update http and rtp modules configuration.

* `http.conf` - asterisk's webserver must accept connection from outside, the listen address must be updated, for the sake of
  simplicity let's use 0.0.0.0, you can also pick an address of one of the network interfaces:

::

    [general]
    enabled=yes
    bindaddr=0.0.0.0
    bindport=5039
    prefix=
    tlsenable=yes
    tlsbindaddr=127.0.0.1:5040
    tlscertfile=/usr/share/xivo-certs/server.crt
    tlsprivatekey=/usr/share/xivo-certs/server.key
    servername=XiVO PBX

Do not forget to reload the configuration by the `module reload http` command on the Asterisk CLI.

* `rtp.conf` - the ICE support must be activated:

::

    ;
    ; RTP Configuration
    ;
    [general]
    ;
    ; RTP start and RTP end configure start and end addresses
    ;
    ; Defaults are rtpstart=5000 and rtpend=31000
    ;
    rtpstart=10000
    rtpend=20000
    ;
    ; Whether to enable or disable UDP checksums on RTP traffic
    ;
    ;rtpchecksums=no
    ;
    ; The amount of time a DTMF digit with no 'end' marker should be
    ; allowed to continue (in 'samples', 1/8000 of a second)
    ;
    ;dtmftimeout=3000
    icesupport=yes
    stunaddr=stun.l.google.com:19302

The configuration is reloaded by `module reload res_rtp_asterisk.so`.

* `sip.conf` - You must generate the DTLS certificates following instructions on the Asterisk Wiki:
  https://wiki.asterisk.org/wiki/display/AST/Secure+Calling+Tutorial. You just need to generate the DTLS certificates,
  other steps are not necessary.
* `Configure the line of the webrtc user` - You must configure your user line as below, so that it is usable with the softphone WebRTC

General :
Set Encryption to Yes

.. figure:: webrtc_line_general.png
    :scale: 100%

Signaling :
Set codec to ulaw

.. figure:: webrtc_line_signalling.png
    :scale: 100%

Advanced :
Set Transport to ws

.. figure:: webrtc_line_advanced.png
    :scale: 100%

Other parameter :

Set the following lines

::

    avpf = yes
    dtlsenable = yes ; Tell Asterisk to enable DTLS for this peer
    dtlsverify = no ; Tell Asterisk to not verify your DTLS certs
    dtlscertfile=/etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS cert file is
    dtlsprivatekey = /etc/asterisk/keys/asterisk.pem ; Tell Asterisk where your DTLS private key is
    dtlssetup = actpass ; Tell Asterisk to use actpass SDP parameter when setting up DTLS
    force_avp = yes
    icesupport = yes

.. figure:: webrtc_line_optional_parameters.png
    :scale: 100%

