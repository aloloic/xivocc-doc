#####################################
Installation and system configuration
#####################################

The XiVO-CC software suite is made of several independent components. Depending on your system size,
they can be installed on separate virtual or physical machines. In this section, we will explain how to
install these components on a single machine.

.. toctree::
   :maxdepth: 2

   installation/installation
   pack_reporting/index
   phone_integration/index
   third_party/index

